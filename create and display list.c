
#include <stdlib.h>

void create();
void display();

struct node
{
   int info;
   struct node *next;
};

struct node *start = NULL;

int main()
{
   int choice;
   while (1)
   {
      printf("1. Create \n");
      printf("2. Display \n");
      printf("3. Exit \n\n");
      printf("Enter your choice: ");
      scanf("%d", &choice);
      switch (choice)
      {
      case 1:
         create();
         break;
      case 2:
         display();
         break;
      case 3:
         return 0;

      default:
         printf("\nWrong Choice.\n\n");
         break;
      }
   }
   return 0;
}
void create()
{
   struct node *temp, *ptr;
   temp = (struct node *)malloc(sizeof(struct node));
   if (temp == NULL)
   {
      printf("\nOut of Memory Space.\n");
      exit(0);
   }
   printf("\nEnter the data value for the node: ");
   scanf("%d", &temp->info);
   temp->next = NULL;
   if (start == NULL)
      start = temp;
   else
   {
      ptr = start;
      while (ptr->next != NULL)
         ptr = ptr->next;
      ptr->next = temp;
   }
   printf("\nData inserted into the list.\n");
   printf("\n");
}
void display()
{
   struct node *ptr;
   if (start == NULL)
   {
      printf("\nList is empty.\n");
      return;
   }
   else
   {
      ptr = start;
      printf("\nThe List elements are: ");
      while (ptr != NULL)
      {
         printf("%d    ", ptr->info);
         ptr = ptr->next;
      }
   }
   printf("\n\n");
}